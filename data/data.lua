-- COROUTINE

Script = coroutine.create(Runscript)

function Resume()
	coroutine.resume(Script);
end

function Pause()
	coroutine.yield(Script);
end

-- METHODS

function Quit()
	DoCall("Quit");
end

function Wait(int)
	DoCall("Wait", int);
	Pause();
end

function SetFlag(string, value)
	DoCall("SetFlag", string, value);
end

function GetFlag(string)
	return (DoCall("GetFlag", string));
end

function FaceMsg(string1, string2)
	DoCall("FaceMsg", string1, string2);
	Pause()
end

function Msg(string)
	DoCall("Msg", string);
	Pause();
end

function InstantFade(int)
	DoCall("InstantFade", int);
end

function Fade(int)
	DoCall("Fade", int);
end

function PlayerActive(bool)
	DoCall("PlayerActive", bool);
end

function HaltFade()
	PlayerActive(0);
	DoCall("HaltFade");
end

function ResumeFade(string)
	PlayerActive(1);
	DoCall("ResumeFade", string);
end
function Runscript()

	if(GetFlag("ShowMsg") ~= "1") then

		HaltFade();

		FaceMsg("info.png", "Press 'Shift' to progress through messages\nThis script is set to load as this level loads.\n\nYou can choose which script runs when a\nlevel loads by adding an 'InstantScript'\nobject.");

		FaceMsg("info.png", "If you have any questions don't hesitate to ask\nme at contact@zephni.com.");

		ResumeFade();

		SetFlag("ShowMsg", "1");
		
	end

end